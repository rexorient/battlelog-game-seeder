this.manifest = {
    "name": "BattleLog Game Seeder",
    "icon": "../icons/BLGS_64.png",
    "settings": [
        {
            "tab": "Information",
            "group": "About",
            "type": "description",
            "text": '\
<p>\
 This Google Chrome plugin extends EA BattleLog to automagically seed servers.  Seeding can be enabled / disabled with a simple toggle.\
 When enabled, the plugin checks the configured servers and finds one which requires seeding.  It then launches the game so the user logged\
 into Origin / BattleLog is placed in the game.  When the server reaches the required number of players, the plugin starts looking for\
 another server to join.\
</p>'
        },
        {
            "tab": "Information",
            "group": "Credits",
            "type": "description",
            "text": 'First versions were based on <a href="https://chrome.google.com/webstore/detail/bf4-auto-seeding/aghnodmijihhdkdilljanmmbmccahbcf"><b>BF4 Auto Seeding</b></a> plugin, written by Kami.  As of v1.2.0, this extension no longer contains any of his code.'
        },
        {
            "tab": "Information",
            "group": "History",
            "type": "description",
            "text": "\
<b>v1.3.0 - 2014-11-29</b>\
<ul>\
 <li> Switching to another server doesn't work reliably when already connected to a server.  Changed to always kill the current server before joining a new one.\
</ul>\
<b>v1.2.0 - 2014-11-13</b>\
<ul>\
 <li> Now supports BF4.\
 <li> Complete rewrite of the seeding engine.  Now able to stop seeding, switch between BF3 and BF4, etc.\
 <li> Added parameters: stop (player count when to stop seeding) and priority (higher priority get seeded first.)</li>\
 <li> Included two example config files, available from the Options -> Documentation page.\
 <li> Includes new icons.</li>\
</ul>\
<b>v1.1 - 2014-11-06</b>\
<ul>\
 <li> Change name of servers selected for seeding to RED.\
 <li> Introduced extension Options page.  This gives a lot more information than the old Version popup did and adds a proper configuration page.\
</ul>\
<b>v1.0 - 2014-11-04</b>\
<ul>\
 <li> Initial rewrite of <b>BF4 Game Seeding</b> plugin for BF3.\
 <li> Rather than spend time getting sorting and configuration working in the BF3 UI, drop this code and load configuration from URL.\
</ul>"
        },
        {
            "tab": "Configuration",
            "group": "Settings",
            "name": "configURI",
            "type": "text",
            "label": "Config URI",
            "text": "e.g. http://server.com/seeder_config.json"
        },
        {
            "tab": "Configuration",
            "group": "Settings",
            "name": "test",
            "type": "button",
            "text": "Test"
        },
        {
            "tab": "Documentation",
            "group": "Installation",
            "type": "description",
            "text": 'To install this Chrome Extention, find it in the Google Play store, and select "FREE".   Since you are reading this, you have probably already done this :-)'
        },
        {
            "tab": "Documentation",
            "group": "Configuration",
            "type": "description",
            "text": "\
Go to the Chrome Extensions page (chrome://extensions), find the BattleLog Game Seeder extension, and select Options.\
On the Options page, go to the Configuration tab and enter the Configuration URI as provided by the Server or Clan administrator.\
Click the TEST button.  If the provided URI is correct, you will see a message informing you how many servers the configuration contains."
        },
        {
            "tab": "Documentation",
            "group": "Usage",
            "type": "description",
            "text": "\
Load BattleLog into 1 (ONE) tab in your browser.  Servers which are in the seeding configuration will show up in RED in the favourites, server browser, and individual server pages.<br/><br/>\
Click SEED in the page menu to start seeding.  SEED will change to SEEDING to indicate seeding is active.  Click SEEDING to stop seeding.  SEEDING will change to SEED to indicate seeding is disabled."
        },
        {
           "tab": "Documentation",
           "group": "Server Config",
           "type": "description",
           "text": "\
Publish a configuration file in a location which can be accessed using a simple URL and does not require authentication.  This is the URL the extention \
users enter in their configuration.  The configuration file contains the list of servers to seed, and should be fairly self-explanatory (if not,\
look for the YouTube manual following shortly.)   Examples: <a href='../config/IMA.json'>BF4 Config</a> and <a href='../config/HostileTakeover.json'>BF3 Config</a>."
        },
    ],
    "alignment": [
    ]
};
