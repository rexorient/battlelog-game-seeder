var storage = chrome.storage.local;

window.addEvent("domready", function () {
    // Option 1: Use the manifest:
    new FancySettings.initWithManifest(function (settings) {
        settings.manifest.test.addEvent("action", function () {
			var uri = settings.manifest.configURI.get();
			var configURI;
			if(uri.indexOf("/") == 0)
				configURI = chrome.extension.getURL(uri);
			else
				configURI = uri;

			var xhr = new XMLHttpRequest();
			xhr.open("GET", configURI, true);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					if(xhr.status == 200) {
						try {
							var config = JSON.parse(xhr.responseText);
							var count = 0;
							if(config.servers != null)
								count = config.servers.length;
							alert("Successfully retrieved configuration with " + count + " servers.");
						}
						catch(e) {
							alert("Parsing retrieved configuration failed with: " + e.message);
						}
					}
					else
						alert("Retrieving configuration failed with status code " + xhr.status);
				}
			};
			xhr.send();
		});

		storage.get("ConfigURI", function(result) {
			settings.manifest.configURI.set(result["ConfigURI"]);
		});

		settings.manifest.configURI.addEvent("action", function() {
			var obj = {}
			obj["ConfigURI"] = settings.manifest.configURI.get();
			storage.set( obj );
		});
	});
});
