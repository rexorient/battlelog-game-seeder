/*****************************************************************************
 ***
 *** BattleLog Game Seeder
 ***
 *** Content Script
 ***
 *** Copyright 2014 Alain Penders (alain@rexorient.com)
 ***
 *****************************************************************************/

var storage = chrome.storage.local;

var EVENT_STATE_REQUEST = "BLGS_EVENT_STATE_REQUEST";
var EVENT_STATE_RESPONSE = "BLGS_EVENT_STATE_RESPONSE";
var EVENT_LAUNCH = "BLGS_EVENT_LAUNCH";
var EVENT_KILL_GAME = "BLGS_EVENT_KILL_GAME";

var seeding = false;
var gameStatus;

var config;
var configURI;

var lastUrl;

var games = {
	BF3 : 2,
	WARSAW : 2048,
	MOHW : 4096,
	BFH : 8192
};

var states = {
	NONE: "NONE",
	READY: "READY",
	PENDING: "PENDING",
	GAMEISGONE: "GAMEISGONE",
	RESERVING_SLOT: "RESERVING_SLOT",
	NA: "State_NA",
	ERROR: "State_Error",
	STARTING: "State_Starting",
	INIT: "State_Init",
	NOT_LOGGED_IN: "State_NotLoggedIn",
	MENU_READY: "State_MenuReady",
	MATCHMAKING: "State_Matchmaking",
	MATCHMAKE_RESULT_HOST: "State_MatchmakeResultHost",
	MATCHMAKE_RESULT_JOIN: "State_MatchmakeResultJoin",
	CONNECT_TO_GAMEID: "State_ConnectToGameId",
	CONNECT_TO_USERID: "State_ConnectToUserId",
	CREATE_COOP_PEER: "State_CreateCoOpPeer",
	MATCHMAKE_COOP: "State_MatchmakeCoOp",
	RESUME_CAMPAIGN: "State_ResumeCampaign",
	LAUNCH_PLAYGROUND: "State_LaunchPlayground",
	WEAPON_CUSTOMIZATON: "State_WeaponCustomization",
	LOAD_LEVEL: "State_LoadLevel",
	CONNECTING: "State_Connecting",
	WAIT_FOR_LEVEL: "State_WaitForLevel",
	GAME_LOADING: "State_GameLoading",
	GAME: "State_Game",
	GAME_LEAVING: "State_GameLeaving",
	IN_QUEUE: "State_InQueue",
	WAIT_FOR_PEER_CLIENT: "State_WaitForPeerClient",
	PEER_CLIENT_CONNECTED: "State_PeerClientConnected",
	CLAIM_RESERVATION: "State_ClaimReservation",
	GAME_READY: "State_Ready"
};

$(document).ready(function () {

	console.log("BattleLog Game Seeder -- Initializing");

	// Add the Seed button to the menu bar
	if (!$("#SeedButton").length) {
		$(".base-section-menu").append("<li data-page='Seed'><a class='wfont' ID='SeedButton' style='cursor:pointer'>Seed</a></li>");

		$("#SeedButton").on("click", function () {
			seeding = !seeding;
			saveSeedingConfiguration();
			updateSeedingStatus();
		});
	}

	// Record starting URL
	lastUrl = document.location.href;

	// Register event listeners
	window.addEventListener(EVENT_STATE_RESPONSE, statusHandler);

	// Inject our code so we can communicate with the BattleLog API
	injectCode();

	// Load our configuration
	loadConfiguration();

	// Start status and href polling
	statusPoll();
	hrefPoll();
});

function updateSeedingStatus() {
	$("#SeedButton").html(seeding ? "Seeding" : "Seed");
	console.log("Seeding " + (seeding ? "enabled" : "disabled"));
	if (seeding) {
		seedingLoop();
	}
}

function hrefPoll() {
	setTimeout(hrefPoll, 1000);

	if (document.location.href != lastUrl) {
		lastUrl = document.location.href;

		// Markup after the page has had some time to load
		setTimeout(function () {
			markupServers();
		}, 1000);
	}
}

function statusPoll() {
	setTimeout(statusPoll, 15000);

	var event = document.createEvent("CustomEvent");
	event.initCustomEvent(EVENT_STATE_REQUEST, true, true, {});
	window.dispatchEvent(event);
}

function injectCode() {

	// TODO: Add code to prevent double injection
	if ($("#blgsInjectedScript").length) {
		console.log("Already injected!");
		return;
	}

	// Inject code into BattleLog so we can interact with its API
	var script = document.createElement('script');
	script.id = "blgsInjectedScript";

	// Appending text to a function to convert it's src to string only works in Chrome
	script.textContent = '(' + function () {

		var EVENT_STATE_REQUEST = "BLGS_EVENT_STATE_REQUEST";
		var EVENT_STATE_RESPONSE = "BLGS_EVENT_STATE_RESPONSE";
		var EVENT_LAUNCH = "BLGS_EVENT_LAUNCH";
		var EVENT_KILL_GAME = "BLGS_EVENT_KILL_GAME";

		window.addEventListener(EVENT_LAUNCH, function (e) {
			var guid = e.detail.guid;
			var game = e.detail.game;
			gamemanager.joinServerByGuid(guid, platforms.PC, game, null, gameServerRoleTypes.SOLDIER);
		});

		window.addEventListener(EVENT_KILL_GAME, function (e) {
			var game = e.detail.game;
			launcher.killGame(game, 0, function (result) {
				console.log("killGame result:");
				console.log(result);
			});
		});

		window.addEventListener(EVENT_STATE_REQUEST, function (e) {
			//console.log(launcher);
			//console.log(gamemanager);

			var result = {};
			result["state"] = launcher.currentState;
			if (gamemanager.launcherState && gamemanager.launcherState.gameState) {
				result["currentGame"] = gamemanager.launcherState.currentGame;
				result["currentLevel"] = gamemanager.launcherState.currentLevel;
				result["gameState"] = gamemanager.launcherState.gameState;
				result["gameStatePretty"] = gamemanager.launcherState.gameStatePretty;
			}
			else {
				result["currentGame"] = "";
				result["currentLevel"] = "";
				result["gameState"] = "";
				result["gameStatePretty"] = "";
			}
			if (gamemanager.launcherState && gamemanager.launcherState.server) {
				result["serverCountry"] = gamemanager.launcherState.server.country;
				result["serverGame"] = gamemanager.launcherState.server.game;
				result["serverGameId"] = gamemanager.launcherState.server.gameId;
				result["serverGuid"] = gamemanager.launcherState.server.guid;
				result["serverIp"] = gamemanager.launcherState.server.ip;
				result["serverMap"] = gamemanager.launcherState.server.map;
				result["serverMapMode"] = gamemanager.launcherState.server.mapMode;
				result["serverName"] = gamemanager.launcherState.server.name;
			}
			else {
				result["serverCountry"] = "";
				result["serverGame"] = "";
				result["serverGameId"] = "";
				result["serverGuid"] = "";
				result["serverIp"] = "";
				result["serverMap"] = "";
				result["serverMapMode"] = "";
				result["serverName"] = "";
			}
			if (gamemanager.launcherState && gamemanager.launcherState.server && gamemanager.launcherState.server.extendedInfo) {
				result["serverBannerUrl"] = gamemanager.launcherState.server.extendedInfo.bannerUrl;
				result["serverDescription"] = gamemanager.launcherState.server.extendedInfo.desc;
				result["serverMessage"] = gamemanager.launcherState.server.extendedInfo.message;
			}
			else {
				result["serverBannerUrl"] = "";
				result["serverDescription"] = "";
				result["serverMessage"] = "";
			}
			var event = document.createEvent("CustomEvent");
			event.initCustomEvent(EVENT_STATE_RESPONSE, true, true, result);
			window.dispatchEvent(event);
		});

	} + ')();'

	// Inject our JS code
	console.log(script);
	(document.head || document.documentElement).appendChild(script);
}

function statusHandler(e) {
	console.log("Status received...");
	console.log(e.detail);
	gameStatus = e.detail;
}

function launchGame(server) {
	var event = document.createEvent("CustomEvent");
	event.initCustomEvent(EVENT_LAUNCH, true, true, {"guid": server.guid, "game": server.game});
	window.dispatchEvent(event);
}

function killGame(game) {
	var event = document.createEvent("CustomEvent");
	event.initCustomEvent(EVENT_KILL_GAME, true, true, {"game": game});
	window.dispatchEvent(event);
}

function seedingLoop() {
	if (seeding) {
		setTimeout(seedingLoop, 30000);

		console.log("seedingLoop() -- Start");

		//if ($(".icon-dialog-close").length > 0) {
		//	$(".icon-dialog-close").get(0).click();
		//}

		if((typeof gameStatus ==="undefined")|| (typeof config === "undefined") || (typeof config.servers === "undefined")) {
			console.log("seedingLoop() -- Aborting due to lack of data!");
			return;
		}

		privateSeedingLoop();
		loadPlayerCounts();

		console.log("seedingLoop() -- Done");
	}
}

function privateSeedingLoop() {
	var idle =
			gameStatus.state == "" ||
			gameStatus.state == states.NONE ||
			gameStatus.state == states.ERROR ||
			gameStatus.state == states.GAMEISGONE;
	console.debug("seedingLoop() -- Idle: " + idle);

	// Determine which game to seed
	var selectedServer = null;
	config.servers.forEach(function (server) {

		console.debug("seedingLoop() -- Now checking: " + server.guid + " / " + server.name);

		// If this server doesn't have a player count yet, skip it.
		if (server.players == -1)
			return;

		// Is it eligible for seeding?
		if (server.players >= server.max || server.players < server.min)
			return;

		// It can only replace a selected server if it has higher priority
		if (selectedServer && selectedServer.priority >= server.priority)
			return;

		selectedServer = server;
	});

	if(selectedServer) {
		console.debug("seedingLoop() -- Best seeding candidate: " + selectedServer.guid + " / " + selectedServer.name);

		// If the selected server is the one we're currently on, bail out.
		if (!idle && (gameStatus.serverGuid == selectedServer.guid)) {
			console.log("seedingLoop() -- Already on right server.");
			return;
		}

		// If we're on a different server.  Find it.
		if(!idle) {
			var currentServer = findServer(gameStatus.serverGuid);

			var switchToSelected = false;
			if (currentServer) {
				if (currentServer.priority < selectedServer.priority) {
					console.log("seedingLoop() -- Switching to new server as it has a higher priority.");
					switchToSelected = true;
				}
				else if (currentServer.players < currentServer.stop) {
					console.log("seedingLoop() -- Switching to new server as the current one has reached its stop count.");
					switchToSelected = true;
				}
			}
			else {
				console.log("seedingLoop() -- Switching to new server as we can't identify the current server.");
				switchToSelected = true;
			}

			if (!switchToSelected) {
				console.log("seedingLoop() -- Not switching as current server has higher priority or hasn't reached its stop-seeding player count.");
				return;
			}
		}

		//if(!idle && (selectedServer.game != gameStatus.currentGame)) {
		//	console.log("seedingLoop() -- Server we are switching to has a different game type.  Killing current game.");
		//	killGame(gameStatus.currentGame);
		//	return;
		//}

		if(!idle) {
			console.log("seedingLoop() -- Need to switch to another server, killing current game.");
			killGame(gameStatus.currentGame);
			return;
		}

		console.log("seedingLoop() -- Launching " + selectedServer.name);
		launchGame(selectedServer);
	}
	else {
		// No server selected for seeding
		if(idle) {
			console.log("seedingLoop() -- Nothing to seed.");
			return;
		}

		var currentServer = findServer(gameStatus.serverGuid);
		if(currentServer) {
			if (currentServer.players >= currentServer.stop) {
				console.log("seedingLoop() -- Current server has reached stop count, exiting game.");
				killGame(currentServer.game);
			}
			else
				console.log("seedingLoop() -- Current server has not reached stop count, leaving it.");
		}
		else
			console.log("seedingLoop() -- Can't identify current server, leaving it.");
	}
}

function loadPlayerCounts() {
	if(config && config.servers) {
		config.servers.forEach(function(server) {
			loadPlayerCount(server)
		});
	}
}

function loadPlayerCount(server) {
	var href;

	switch(server.game) {
		case games.BF3:
			href = 'http://battlelog.battlefield.com/bf3/servers/show/pc/' + server.guid + " #server-info-players";
			break;

		case games.WARSAW:
			href = 'http://battlelog.battlefield.com/bf4/servers/show/pc/' + server.guid + " #server-page-info > div.box:first > section > h5";
			break;

		default:
			server.players = -1;
			return;
	}

	// Create player count div
	//noinspection JSJQueryEfficiency
	var serverDiv = $("#Server" + server.guid);
	if (!serverDiv.length) {
		$("body").append('"<div id="Server' + server.guid + '" guid="' + server.guid + '" style="display:none"></div>');
		serverDiv = $("#Server" + server.guid);
	}

	// Load count data
	serverDiv.load(href, function () {
		var guid = $(this).attr("guid");
		var server = findServer(guid);
		if(server) {
			server.players = getPlayerCount(server.game, guid);
			console.log("Player count for " + server.name + " set to " + server.players);
		}
	});
}

function getPlayerCount(game, guid) {
	switch(game) {
		case games.BF3:
			var bf3data = $.trim($("#Server" + guid + " td").html());
			if (bf3data.indexOf("/") == -1) {
				// Not available yet!
				return -1;
			}
			return parseInt(bf3data.split('/', 1));
			break;

		case games.WARSAW:
			var bf4data = $.trim($("#Server" + guid + " h5").html());
			if (bf4data.indexOf("/") == -1) {
				// Not available yet!
				return -1;
			}
			return parseInt(bf4data.split('/', 1));
			break;

		default:
			return -1;
	}
}

function loadConfiguration() {
	storage.get({ "ConfigURI": "", "Seeding": false}, function (result) {
		var uri = result["ConfigURI"];
		seeding = result["Seeding"];
		if(seeding)
			updateSeedingStatus();

		if (uri == null || uri == "") {
			window.alert("Configuration URI isn't set.  Open the Extension options page and set it!");
			// TODO: Open options.
			return;
		}

		if (uri.indexOf("http") == 0)
			configURI = uri;
		else
			configURI = chrome.extension.getURL(uri);

		// Create a place to load
		var configDiv = $("#Seeding-Config");
		if (!configDiv.length) {
			$("body").append("<div id='Seeding-Config' style='display:none'></div>");
			configDiv = $("#Seeding-Config");
		}

		// Load it
		configDiv.load(configURI, function () {
			console.log("Config loaded!");

			var hold = $("#Seeding-Config").html();
			config = $.parseJSON(hold);

			if(config && config.servers) {
				config.servers.forEach(function (server) {
					// Set game
					server.game = nameToType(server.type);

					// Unknown player count
					server.players = -1;

					if(typeof server.priority === "undefined")
						server.priority = 0;

					// Set Name if needed
					if (!server.name)
						server.name = server.guid;

					// Log it
					console.debug(server.type +
					              " - Name: " + server.name +
					              " ID: " + server.guid +
					              " Min: " + server.min +
					              " Max: " + server.max +
					              " Stop: " + server.stop +
					              " Game: " + server.game +
					              " Priority: " + server.priority);
				});
			}

			markupServers();
		});
	});
}

function markupServers() {
	switch(gameFromUri()) {
		case games.BF3:
			markupServersBF3();
			return;

		case games.WARSAW:
			markupServersBF4();
			break;

		case games.BFH:
			// Gamble that this might work
			markupServersBF4();
			break;

		case games.MOHW:
		default:
			return;
	}
}

function markupServersBF3() {
	if(!config || !config.bf3_servers)
		return;

	config.servers.forEach(function (server) {
		if(server.game != games.BF3)
			return;

		var fav = $('div.serverguide-cell-name-server-wrapper > a[href^="/bf3/servers/show/pc/' + server.guid + '"]');
		if(fav) {
			fav.css("color", "red");
		}

		var header = $("#server-header");
		if(header) {
			if(header.find('button[data-guid="' + server.guid + '"]').length > 0) {
				console.log("FOUND");
				console.log(server);
				header.find("h1").css("color", "red");
			}
		}
	});
}

function markupServersBF4() {
	if(!config || !config.bf4_servers)
		return;

	config.servers.forEach(function (server) {
		if(server.game != games.WARSAW)
			return;

		var fav = $('tr.server-row[data-guid="' + server.guid + '"] > td.server > div.server-name');
		if(fav) {
			fav.css("color", "red");
		}

		var header = $('#server-page[data-guid="' + server.guid + '"]');
		if(header) {
			header.find("h1").first().css("color", "red");
		}
	});
}

function gameFromUri() {
	var uri = document.location.href;
	if(uri.indexOf("/bf3/") != -1)
		return games.BF3;
	if(uri.indexOf("/bf4/") != -1)
		return games.WARSAW;
	if(uri.indexOf("/mohw/") != -1)
		return games.MOHW;
	if(uri.indexOf("/bfh/") != -1)
		return games.BFH;

	return 0;
}

function findServer(guid) {
	for(var i = 0; i < config.servers.length; i++) {
		var server = config.servers[i];
		if(server.guid == guid)
			return server;
	}

	return null;
}

function nameToType(name) {
	var typeName = name.toLowerCase();

	if(typeName == "bf3")
		return games.BF3;
	if(typeName == "bf4")
		return games.WARSAW;
	if(typeName == "bfh")
		return games.BFH;
	if(typeName == "mohw")
		return games.MOHW;
	return 0;
}

function typeToName(type) {
	if(type == games.BF3)
		return "BF3";
	if(type == games.WARSAW)
		return "BF4";
	if(type == games.BFH)
		return "Hardline";
	if(type == games.MOHW)
		return "MOHW";
	return "Unknown";
}

function saveSeedingConfiguration() {
	var obj = {}
	obj["Seeding"] = seeding;
	storage.set( obj );
}
